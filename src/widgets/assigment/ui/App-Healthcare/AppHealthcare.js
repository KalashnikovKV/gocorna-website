import './AppHealthcare.css';

export function AppHealthcare(props){
    const container=document.createElement('div');
    container.classList.add('healthcareDiv');
    const healthcareTitle = document.createElement('div');
    healthcareTitle.classList.add('healthcareTitle');
    healthcareTitle.innerHTML=('<div class="textTitleHealthcare"><span class="redTextHealthcare">Healthcare </span><span class="blackTextHealthcare">at your Fingertips.</span></div><div class="subtitleHealthcare">Bringing premium healthcare features to your fingertips. User friendly mobile platform to bring healthcare to your fingertips. Signup and be a part of the new health culture.</div>')
    const healthcareImgsDiv = document.createElement('div');
    healthcareImgsDiv.classList.add('healthcareImgsDiv');
    // healthcareImgsDiv.innerHTML = ('<img src="../assets/images/Group\ 28.png">')
    const leftHeathcareImg = document.createElement('span');
    leftHeathcareImg.classList.add('leftHeathcareImg');
    leftHeathcareImg.innerHTML=('<img src="../assets/icons/Group 42.svg">')

    const midHeathcareImg = document.createElement('span');
    midHeathcareImg.classList.add('midHeathcareImg');
    midHeathcareImg.innerHTML=('<img src="../assets/icons/Group 43.svg">')
 
    const rightHeathcareImg = document.createElement('span');
    rightHeathcareImg.classList.add('rightHeathcareImg');
    rightHeathcareImg.innerHTML=('<img src="../assets/icons/Group 45.svg">')

    const healthcareBtm = document.createElement('div');
    healthcareBtm.classList.add('healthcareBtm');
    healthcareBtm.innerHTML=('<img class="imgHealthcareBtmLeft" src="../assets/images/image\ 2.png"><img class="imgHealthcareBtmRight" src="../assets/images/image\ 1.png">');

    container.append(healthcareTitle);
    container.append(healthcareImgsDiv);
    healthcareImgsDiv.append(leftHeathcareImg);
    healthcareImgsDiv.append(midHeathcareImg);
    healthcareImgsDiv.append(rightHeathcareImg);
    container.append(healthcareBtm);
    return container;
}
