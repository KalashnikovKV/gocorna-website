import './AppTalk.css';
import { AppCard } from "../../../../components/App-card/AppCard";

export function AppTalk(props){
const container = document.createElement('div');
container.classList.add("TalkDiv");
 
const outerTittleDiv = document.createElement('div');
outerTittleDiv.classList.add('outerTittleDiv'); 
outerTittleDiv.innerHTML=('<img src="../assets/images/Group\ 28.png">')
const tittleDiv = document.createElement('div');
tittleDiv.classList.add("tittleDiv");

const leftTitleDiv = document.createElement('span');
leftTitleDiv.classList.add('leftTitleDiv');
leftTitleDiv.innerHTML=('<span class="redText">2 m</span><br><span class="blackText">USERS</span>');

const centrTitleDiv = document.createElement('span');
centrTitleDiv.classList.add('centrTitleDiv');
centrTitleDiv.innerHTML=('<span class="redText">78</span><br><span class="blackText">COUNTRIES</span>');

const rightTitleDiv = document.createElement('span');
rightTitleDiv.classList.add('rightTitleDiv');
rightTitleDiv.innerHTML=('<span class="redText">10,000+</span><br><span class="blackText">MEDICAL EXPERTS</span>');

const mainDiv = document.createElement('div');
mainDiv.classList.add('mainDivTalk');

const imgTalk = document.createElement('span');
imgTalk.classList.add('imgTalk');

const innerImgTalk = document.createElement('span');
innerImgTalk.classList.add('innerImgTalk')
innerImgTalk.innerHTML=('<img src="../assets/images/Group\ 15.png">')
 const playIcon = document.createElement('span');
 playIcon.classList.add('playIcon');
 playIcon.innerHTML=('<img src="../assets/icons/Group\ 23.png">')
 

container.append(outerTittleDiv);
outerTittleDiv.append(tittleDiv);
tittleDiv.append(leftTitleDiv);
tittleDiv.append(centrTitleDiv);
tittleDiv.append(rightTitleDiv);
container.append(mainDiv);
mainDiv.append(AppCard({color:'--color-blue',Title:'Talk to', ColoredText:'experts', Subtitle:'Book appointments or submit queries into thousands of forums concerning health issues and prevention against noval Corona Virus.',btnText:'FEATURES',btnMode:'primary'}));
mainDiv.append(imgTalk);
imgTalk.append(innerImgTalk);
innerImgTalk.append(playIcon);

return container;
}
