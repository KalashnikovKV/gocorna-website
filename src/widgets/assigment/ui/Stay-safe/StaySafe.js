import "./StaySafe.css";
import { AppButton } from "../../../../components/app-button/AppButton";
import { AppCard } from "../../../../components/App-card/AppCard";
import img22 from "../../../../../assets/images/Group 22.png";

export function StaySafe() {
  const container = document.createElement("div");
  container.classList.add("staysafe-container");

  const leftContent = document.createElement("div");
  leftContent.classList.add("staysafe-left");

  const rightContent = document.createElement("div");
  rightContent.classList.add("staysafe-right");

  const image = document.createElement("img");
  image.src = img22;
  image.alt = "Stay Safe Image";
  leftContent.appendChild(image);

  const appStaySafeCard = AppCard({
    color: "--color-red",
    Title: "Stay safe with Go",
    ColoredText: "Corona",
    Subtitle:
      "24x7 Support and user-friendly mobile platform to bring healthcare to your fingertips. Signup and be a part of the new health culture.",
    btnText: "Features",
    btnMode: "primary",
  });
  rightContent.appendChild(appStaySafeCard);

  container.appendChild(leftContent);
  container.appendChild(rightContent);

  return container;
}
