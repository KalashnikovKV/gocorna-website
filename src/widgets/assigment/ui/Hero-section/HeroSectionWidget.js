import { AppHeader } from "../../../../components/app-header/AppHeader";
import { AppSectionContentContainer } from "../../../../components/app-section-content-container/appSectionContentContainer";
import { AppVideoLink } from "../../../../components/app-video-link/AppVideoLink";
import imgGr23 from "../../../../../assets/images/Group 23.png";

export function HeroSectionWidget() {
  const widget = document.createElement("div");
  widget.classList.add("hero-container");
  const restangle = document.createElement("div");
  restangle.classList.add("rectangle");

  widget.append(restangle);
  widget.append(AppHeader());
  widget.append(AppSectionContentContainer());
  widget.append(
    AppVideoLink({
      span: "Stay safe with GoCorona",
      p: "Watch Video",
      text: "Group 23",
      srcImage: imgGr23,
    })
  );

  return widget;
}
