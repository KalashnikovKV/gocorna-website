import "./styles.css";
import {AppTalk} from "./widgets/assigment/ui/App-talk/AppTalk";
import { AppMenuNavigation } from "./components/app-menu-navigation/AppMenuNavigation";
import { HeroSectionWidget } from "./widgets/assigment/ui/Hero-section/HeroSectionWidget";
import { StaySafe } from "./widgets/assigment/ui/Stay-safe/StaySafe";
import {AppHealthcare} from "./widgets/assigment/ui/App-Healthcare/AppHealthcare";

const app = document.querySelector("#app"); // div#app

const h1 = document.createElement("h1");
h1.innerText = "Gocorna Website";

app.append(h1);
app.append(HeroSectionWidget());
app.append(StaySafe());
app.append(AppTalk());
app.append(AppHealthcare());


