import './app-card.css';
import { AppButton } from '../app-button/AppButton.js';

export function AppCard(props) {
  const container = document.createElement('div');
  container.classList.add("cardDiv");

  const title = document.createElement('h2');
  title.classList.add("cardH2");
  title.innerText = props.Title;

  const subtitle = document.createElement('h3');
  subtitle.classList.add("cardH3");

  const coloredTitle = document.createElement('span');
  coloredTitle.classList.add('coloredTextRed');
  if (props.color === '--color-blue') {
    coloredTitle.classList.add('coloredTextBlue');
  } else if (props.color === '--color-red') {
    coloredTitle.classList.add('coloredTextRed');
  }
  coloredTitle.innerText = props.ColoredText;

  subtitle.innerText = props.Subtitle;



  container.append(title);
  container.append(subtitle);
  title.append(' ');
  title.append(coloredTitle);
  container.append(AppButton({ text: props.btnText, mode: props.btnMode }));



  return container;
};


