export function AppButton(props) {
  const { text, mode } = props;

  const button = document.createElement("button");

  button.textContent = text;

  if (mode === "primary") {
    button.style.backgroundColor = "rgba(236, 88, 99, 1)";
    button.style.color = "white";
  } else if (mode === "secondary") {
    button.style.backgroundColor = "blue";
    button.style.color = "white";
  }
  // else {
  //   button.style.backgroundColor = "gray";
  //   button.style.color = "black";
  // }

  return button;
}
