export function AppMenuNavigation(props) {
  const container = document.createElement("nav");

  const ul = document.createElement("ul");
  props.items.forEach((itemText) => {
    const li = document.createElement("li");
    const a = document.createElement("a");
    li.classList.add("li-header");
    a.textContent = itemText;
    a.setAttribute("href", itemText.replace(/ /g, "_").toUpperCase());
    // li.addEventListener("click", () => handleItemClick(itemText));
    li.appendChild(a);
    ul.appendChild(li);
  });
  container.appendChild(ul);

  return container;
}
