export function AppVideoLink(props) {
  const containerVideoLink = document.createElement("div");

  const container1 = document.createElement("div");
  container1.classList.add("icon-video-link");
  const a = document.createElement("a");
  a.classList.add("app-vedeo-link-icon");
  a.setAttribute("href", "app-vedeo-link-icon");
  const image = document.createElement("img");
  image.src = props.srcImage;
  image.alt = props.text;
  container1.appendChild(a);
  a.appendChild(image);

  const container2 = document.createElement("div");
  container2.classList.add("describe-video-link");
  const span = document.createElement("span");
  span.textContent = props.span;
  const p = document.createElement("p");
  p.textContent = props.p;

  container2.appendChild(span);
  container2.appendChild(p);

  containerVideoLink.appendChild(container1);
  containerVideoLink.appendChild(container2);
  containerVideoLink.id = "app-video-link";

  return containerVideoLink;
}
