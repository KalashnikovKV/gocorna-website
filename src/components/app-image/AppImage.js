export function AppImage(props) {
  const container = document.createElement("div");
  container.classList.add(props.text.replace(/ /g, "_").toUpperCase());

  const image = document.createElement("img");
  image.src = props.srcImage;
  image.alt = props.text;

  container.appendChild(image);
  container.id = "app-image";

  return container;
}
