import { AppButton } from "../app-button/AppButton";
import { AppMenuNavigation } from "../app-menu-navigation/AppMenuNavigation";
import { AppLogo } from "../app-logo/AppLogo";
import logo from "../../../assets/images/Group 2.png";

export function AppHeader() {
  const container = document.createElement("div");
  container.classList.add("header-container");

  const appLogo = AppLogo({
    image: logo,
    text: "#",
  });
  const menuNavigation = AppMenuNavigation({
    items: ["Home", "FEATURES", "SUPPORT", "CONTACT US"],
  });

  const buttons = [AppButton({ text: "DOWNLOAD", mode: "secondary" })];

  container.append(appLogo);
  container.append(menuNavigation);
  container.append(...buttons);

  return container;
}
