import { AppCard } from "../App-card/AppCard";
import { AppImage } from "../app-image/AppImage";
import imageGr14 from "../../../assets/images/Group 14.png";

export function AppSectionContentContainer() {
  const widget = document.createElement("div");
  widget.classList.add("hero-content-container");

  widget.append(
    AppCard({
      color: "--color-blue",
      Title: "Take care of yours family’s",
      ColoredText: "health.",
      Subtitle:
        "All in one destination for COVID-19 health queries.  Consult 10,000+ health workers about your concerns.",
      btnText: "GET STARTED",
      btnMode: "primary",
    })
  );
  widget.append(
    AppImage({
      text: "Group 14",
      srcImage: imageGr14,
    })
  );

  return widget;
}
