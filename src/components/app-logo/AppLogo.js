export function AppLogo(props) {
  const container = document.createElement("a");

  // <a class="logo" href="#"><img src="./assets/icons/logo.svg" alt="logo">Simo</a>
  container.classList.add("logo");
  container.setAttribute("href", props.text.replace(/ /g, "_").toUpperCase());

  const imgLogo = document.createElement("img");
  imgLogo.src = props.image;
  imgLogo.alt = "logo";

  container.appendChild(imgLogo);

  return container;
}
